﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using HowNow.Hub.Exporter.Clients;
using HowNow.Hub.Exporter.Logging;
using HowNow.Hub.Exporter.Models.Clients;
using HowNow.Hub.Exporter.Models.Records;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;

namespace HowNow.Hub.Exporter
{
    public class Program
    {
        private static async Task Main()
        {
            var services = ConfigureServices();
            var hubClient = new HubClient();

            var serviceProvider = services.BuildServiceProvider();
            await serviceProvider.GetService<Exporter>().Run();
            Log.CloseAndFlush();
        }
        private static IServiceCollection ConfigureServices()
        {
            var services = new ServiceCollection();

            var config = new ConfigurationBuilder()
                .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location))
                .AddJsonFile("appsettings.json")
                .Build();

            services.AddSingleton(config);
            services.AddTransient<Exporter>();

            var logger = InitializeLogger(config);
            Log.Logger = logger;
            services.AddLogging(c => c.AddSerilog(logger));

            return services;
        }

        private static ILogger InitializeLogger(IConfiguration configuration)
        {

            var logConfiguration = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .Enrich.FromLogContext()
                .Enrich.With(new AppEnvironmentEnricher(configuration));

            if (Environment.UserInteractive)
            {
                logConfiguration.WriteTo.ColoredConsole();
            }

            return logConfiguration.CreateLogger();
        }
    }
}
