﻿namespace HowNow.Hub.Exporter.Endpoints
{
    internal class ClientEndpoints
    {
        internal const string ClientList = "v2/client/clientlist";
        internal const string ClientDetailsById = "v2/client/byclientid/{0}";
        internal const string ClientDetailsByCode = "v2/client/byclientcode/{0}";
        internal const string EmployeeList = "/hownow/rest/1/organization";
    }
}