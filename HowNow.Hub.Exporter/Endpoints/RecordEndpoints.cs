﻿namespace HowNow.Hub.Exporter.Endpoints
{
    internal class RecordEndpoints
    {
        internal const string CreateRecord = "v2/records/clientrecord";
        internal const string RecordLocations = "v2/records/recordlocations";
        internal const string ClientRecords = "v2/records/clientrecords/{0}";
        internal const string RecordStatuses = "v2/records/statuses";
        internal const string AllRecords = "v2/records/allrecords";
        internal const string RecordUrl = "v2/records/{0}/url";
    }
}
