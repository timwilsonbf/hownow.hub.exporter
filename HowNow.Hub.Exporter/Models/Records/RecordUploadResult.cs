﻿namespace HowNow.Hub.Exporter.Models.Records
{
    public class RecordUploadResult
    {
        public int RecordId { get; set; }
        public string RecordName { get; set; }
    }
}
