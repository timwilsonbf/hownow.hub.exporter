﻿using System;

namespace HowNow.Hub.Exporter.Models.Records
{
    public class Record
    {
        public int RecordId { get; set; }
        public string Title { get; set; }
        public int LocationId { get; set; }
        public int? SubLocationId { get; set; }
        public string Description { get; set; }
        public string FilePath { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
        public int? Status { get; set; }
        public int? RecordType { get; set; }
        public string JobCode { get; set; }
        public int? JobId { get; set; }
        public DateTime? DateAdded { get; set; }
        public string ContactCode { get; set; }
        public string ContactName { get; set; }
    }
}
