﻿using System.Collections.Generic;

namespace HowNow.Hub.Exporter.Models.Records
{
    public class SimpleRecordLocation
    {
        public string Description { get; set; }
        public int LocationId { get; set; }
        public int GroupId { get; set; }
        public int ParentId { get; set; }
    }
}
