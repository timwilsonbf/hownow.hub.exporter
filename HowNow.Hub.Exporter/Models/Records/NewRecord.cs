﻿namespace HowNow.Hub.Exporter.Models.Records
{
    public class NewRecord
    {
        public string Title { get; set; }
        public int ClientId { get; set; }
        public int LocationId { get; set; }
        public string FilePath { get; set; }
        public int Status { get; set; }
    }
}
