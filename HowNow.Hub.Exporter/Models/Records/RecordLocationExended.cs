﻿using System.Collections.Generic;

namespace HowNow.Hub.Exporter.Models.Records
{
    public class RecordLocation
    {
        public int ColumnId { get; set; }
        public string Description { get; set; }
        public int LocationId { get; set; }
        public int GroupId { get; set; }
        public int RowId { get; set; }
        public List<RecordLocation> SubLocations { get; set; }
    }
}
