﻿namespace HowNow.Hub.Exporter.Models.Records
{
    public class RecordStatus
    {
        public int RecordStatusId { get; set; }
        public string RecordStatusName { get; set; }
    }
}
