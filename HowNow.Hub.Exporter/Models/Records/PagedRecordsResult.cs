﻿using System.Collections.Generic;

namespace HowNow.Hub.Exporter.Models.Records
{
    public class PagedRecordsResult
    {
        public int Total { get; set; }
        public int CurrentPage { get; set; }
        public int MaxPage { get; set; }
        public List<Record> Records { get; set; }
    }
}
