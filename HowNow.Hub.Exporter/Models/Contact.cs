﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hownow.Hub.Client.Models
{
    public class Client
    {
        public int ClientId { get; set; }
        public string EntityName { get; set; }
    }
}
