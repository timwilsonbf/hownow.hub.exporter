﻿namespace HowNow.Hub.Exporter.Models.Clients
{
    public class ClientDetailed
    {
        public int ClientId { get; set; }
        public string ClientCode { get; set; }
        public string Salutation { get; set; }
        public string Title { get; set; }
        public string FirstNames { get; set; }
        public string LastName { get; set; }
        public string EntityName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Addressee { get; set; }
        public string GroupName { get; set; }
        public string IsActive { get; set; }
        public string PartnerInitials { get; set; }
        public string PartnerName { get; set; }
        public string Abn { get; set; }
        public string Acn { get; set; }
        public string ManagerName { get; set; }
        public string ManagerInitials { get; set; }
        public string ReviewerName { get; set; }
        public string ReviewerInitials { get; set; }
    }
}
