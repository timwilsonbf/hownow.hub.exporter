﻿namespace HowNow.Hub.Exporter.Models.Clients
{
    public class Client
    {
        public int ClientId { get; set; }
        public string ClientCode { get; set; }
        public string EntityName { get; set; }
        public string GroupName { get; set; }
    }
}
