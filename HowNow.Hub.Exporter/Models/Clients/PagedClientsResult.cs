﻿using System.Collections.Generic;

namespace HowNow.Hub.Exporter.Models.Clients
{
    public class PagedClientsResult
    {
        public int Total { get; set; }
        public int CurrentPage { get; set; }
        public int MaxPage { get; set; }
        public List<Client> Clients { get; set; }
    }
}
