﻿namespace HowNow.Hub.Exporter.Models.Auth
{
    public class SignInResult
    {
        public bool IsError { get; }
        public string Message { get; }

        public SignInResult(bool isError, string message)
        {
            IsError = isError;
            Message = message;
        }
    }
}
