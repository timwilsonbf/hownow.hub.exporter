﻿namespace HowNow.Hub.Exporter.Models.Auth
{
    public class AccessTokenResult
    {
        public string AccessToken { get; }
        public string Error { get; }
        public bool IsError { get; }

        public AccessTokenResult(string accessToken = null, string error = null)
        {
            AccessToken = accessToken;
            Error = error;
            IsError = string.IsNullOrEmpty(accessToken);
        }
    }
}
