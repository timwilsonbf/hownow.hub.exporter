﻿namespace HowNow.Hub.Exporter.Models.Members
{
    public class Employee
    {
        public int Id { get; set; }
        public string Firstnames { get; set; }
        public string Lastname { get; set; }
        public string Initials { get; set; }
        public string Email { get; set; }
    }
}