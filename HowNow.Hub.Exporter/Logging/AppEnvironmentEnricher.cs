﻿using Microsoft.Extensions.Configuration;
using Serilog.Core;
using Serilog.Events;

namespace HowNow.Hub.Exporter.Logging
{
    public class AppEnvironmentEnricher : ILogEventEnricher
    {
        public const string MachineNamePropertyName = "MachineName";
        public const string EnvironmentPropertyName = "Environment";
        public const string ApplicationNamePropertyName = "ApplicationName";
        public const string ApplicationVersionPropertyName = "ApplicationVersion";

        private readonly string _applicationName;
        private readonly string _environment;

        private LogEventProperty[] _cachedProperties;

        public AppEnvironmentEnricher(IConfiguration configuration)
        {
            _applicationName = configuration["Environment:ApplicationName"];
            _environment = configuration["Environment:Name"];
        }

        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            if (_cachedProperties == null)
            {
                _cachedProperties = new[]
                {

                    propertyFactory.CreateProperty(MachineNamePropertyName, AppEnvironment.MachineName),
                    propertyFactory.CreateProperty(ApplicationVersionPropertyName, AppEnvironment.ApplicationVersion),
                    propertyFactory.CreateProperty(EnvironmentPropertyName, _environment),
                    propertyFactory.CreateProperty(ApplicationNamePropertyName, _applicationName)
                };
            }

            foreach (var property in _cachedProperties)
            {
                logEvent.AddPropertyIfAbsent(property);
            }
        }
    }
}