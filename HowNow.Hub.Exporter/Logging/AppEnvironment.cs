﻿using System;

namespace HowNow.Hub.Exporter.Logging
{
    public class AppEnvironment
    {
        public static string MachineName => Environment.MachineName;
        public static string ApplicationVersion => typeof(AppEnvironment).Assembly.GetName().Version.ToString();
    }
}