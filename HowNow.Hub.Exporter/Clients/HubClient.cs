﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.WebPages;
using HowNow.Hub.Exporter.Endpoints;
using HowNow.Hub.Exporter.Models.Auth;
using HowNow.Hub.Exporter.Models.Clients;
using HowNow.Hub.Exporter.Models.Records;
using HowNow.Hub.Exporter.Constants;
using HowNow.Hub.Exporter.Models.Members;
using Newtonsoft.Json;
using Serilog;

namespace HowNow.Hub.Exporter.Clients
{
    public class HubClient
    {
        private readonly string _apiBaseUrl;
        private readonly AuthClient _authClient;
        private List<RecordLocation> _recordLocationRoots;

        public HubClient (string refreshToken = null, string redirectUri = "http://localhost/")
        {
            _apiBaseUrl = ConfigurationManager.AppSettings["HubApiUrl"];
            _authClient = new AuthClient(refreshToken, redirectUri);
        }

        /// <summary>
        /// Redirects the user to the browser to login
        /// </summary>
        /// <returns>A login result, identifies if the login was successful or if an error occured</returns>
        public async Task<SignInResult> LoginAsync()
        {
            var result = await _authClient.SignIn();

            return result;
        }

        /// <summary>
        /// Gets the record locations
        /// </summary>
        /// <returns>A list of record locations and sub locations</returns>
        public async Task<List<RecordLocation>> GetClientRecordLocations()
        {
            var result = await ProcessWebRequest<List<RecordLocation>>(HttpMethod.Get, RecordEndpoints.RecordLocations);

            _recordLocationRoots = result;

            return _recordLocationRoots;
        }

        private RecordLocation CheckSubLocations(IReadOnlyCollection<RecordLocation> locations, string locationDescription)
        {
            if (locations == null) return null;

            foreach (var location in locations)
            {
                if (string.Equals(location.Description, locationDescription, StringComparison.CurrentCultureIgnoreCase))
                    return location;

                var subLocation = CheckSubLocations(location.SubLocations, locationDescription);

                if (subLocation != null)
                    return subLocation;
            }

            return null;
        }

        public async Task<PagedRecordsResult> GetAllRecords(int page = 1)
        {
            var query = $"?page={page}";
            
            var endpoint = $"{RecordEndpoints.AllRecords}{query}";
            var result = await ProcessWebRequest<PagedRecordsResult>(HttpMethod.Get, endpoint);

            return result;
        }

        /// <summary>
        /// Get a paged list of client IDs
        /// </summary>
        /// <param name="page">The requested page</param>
        /// <param name="groupName">The group name to filter by</param>
        /// <param name="includeInactive">Include inactive clients in request</param>
        /// <param name="pageSize">Allows for the setting of the pagination size</param>
        /// <returns>The total number of clients and a list of clients (250)</returns>
        public async Task<PagedClientsResult> GetAllClientIds(int page = 1, string groupName = null, bool includeInactive = false, int pageSize = 10000)
        {
            var query = $"?page={page}";

            if (!string.IsNullOrEmpty(groupName))
                query += $"&groupName={groupName}";

            query += $"&includeInactive={includeInactive}";
            query += $"&pageSize={pageSize}";

            var endpoint = $"{ClientEndpoints.ClientList}{query}";
            var result = await ProcessWebRequest<PagedClientsResult>(HttpMethod.Get, endpoint);

            return result;
        }

        public async Task<List<Employee>> GetEmployeeDetails()
        {
            var result = await ProcessWebRequest<List<Employee>>(HttpMethod.Get, OrganisationEndpoints.Employees);

            return result;
        }

        private async Task<ClientDetailed> GetClientDetails(string endpoint)
        {
            var result = await ProcessWebRequest<ClientDetailed>(HttpMethod.Get, endpoint);

            return result;
        }

        private async Task<ClientDetailed> GetMemberDetails(string endpoint)
        {
            var result = await ProcessWebRequest<ClientDetailed>(HttpMethod.Get, endpoint);

            return result;
        }

        public async Task<string> DownloadRecordById(int recordId, string filePath)
        {
            var endpoint = string.Format(RecordEndpoints.RecordUrl, recordId);

            var downloadUrl = await ProcessWebRequest<string>(HttpMethod.Get, endpoint);

            if (downloadUrl.IsEmpty()) return string.Empty;

            Log.Information("Downloading Record...");

            if (!Directory.Exists(filePath))
                Directory.CreateDirectory(filePath);

            var index = downloadUrl.IndexOf('?');
            var urlFileName = downloadUrl.Substring(0, index);
            var fileName = Path.GetFileName(urlFileName);
            var fullFileName = filePath + fileName;

            try
            {
                await ProcessDownloadRequest(downloadUrl, fullFileName);
            }
            catch (Exception e)
            {
                Log.Error(e.Message);
                return string.Empty;
            }

            return fullFileName;
        }
        
        private async Task<T> ProcessWebRequest<T>(HttpMethod method, string url,
            HttpContent content = null)
        {
            var request = new HttpRequestMessage(method, new Uri(new Uri(_apiBaseUrl), url));

            var tokenResult = await _authClient.GetAccessToken();

            if (tokenResult.IsError)
                throw new Exception(tokenResult.Error);

            request.Headers.Add(Consts.Authorization, $"{Consts.Bearer} {tokenResult.AccessToken}");

            request.Headers.Accept.Clear();
            request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            if (content != null)
            {
                request.Content = content;
            }

            var response = await new HttpClient().SendAsync(request);

            if (!response.IsSuccessStatusCode)
            {
                var error = await response.Content.ReadAsStringAsync();
                throw new Exception($"{response.StatusCode}: {error.Trim('\"')}");
            }

            var responseString = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<T>(responseString);

            return result;
        }
        private async Task ProcessDownloadRequest(string url, string filepath)
        {
            using (var client = new WebClient())
            {
                client.DownloadFile(url, filepath);
            }
        }
    }
}
