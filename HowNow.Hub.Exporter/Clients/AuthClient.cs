﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using HowNow.Hub.Exporter.Constants;
using HowNow.Hub.Exporter.Models.Auth;
using IdentityModel.OidcClient;

namespace HowNow.Hub.Exporter.Clients
{
    internal class AuthClient
    {
        public readonly string FirmId;

        private readonly string _authority = ConfigurationManager.AppSettings["IdentityServerUrl"];
        private readonly string _scope = ConfigurationManager.AppSettings["Scope"];
        private readonly string _clientId = ConfigurationManager.AppSettings["ClientId"];
        private readonly string _redirectUri;

        private readonly OidcClient _oidcClient;
        private string RefreshToken { get; set; }
        private string AccessToken { get; set; }
        private string IdToken { get; set; }

        private const string ResponseHtml =
            @"<!doctype html>
                <html>
                    <head>
                        <style>
                            .center {
                                padding-top: 3rem;
                                text-align: center;
                                margin: 0 auto;
                            }
                            .spacer-md {
                                height: 1.5rem;
                            }
                            body {
                                font-family: ""Open Sans"", sans-serif;
                                font-size: 0.9rem;
                                line-height: 1.5;
                                font-weight: 400;
                                color: #212529;
                            }
                        </style>
                    </head>
                    <body>
                        <div class=""center"">
                            <img src = ""https://bfsoftware.blob.core.windows.net/assets/bf-header-logo-001.png"" width=""160"">
                            <div class=""spacer-md""></div>
                            <p>
                                <b>Successfully Logged In</b>
                            </p>
                            <div class=""spacer-md""></div>
                            <p> Please close this tab and return to the application </p>
                        </div>
                    </body>
                </html>";

        internal AuthClient (string refreshToken, string redirectUri)
        {
            _redirectUri = "http://localhost:4200/";

            var options = new OidcClientOptions
            {
                Authority = _authority,
                ClientId = _clientId,
                Scope = _scope,
                RedirectUri = _redirectUri,
                Flow = OidcClientOptions.AuthenticationFlow.AuthorizationCode,
            };

            _oidcClient = new OidcClient(options);

            RefreshToken = refreshToken;
        }

        internal async Task<SignInResult> SignIn()
        {
            var http = new HttpListener();
            http.Prefixes.Add(_redirectUri);
            http.Start();

            var state = await _oidcClient.PrepareLoginAsync();

            // open system browser to start authentication
            Process.Start(state.StartUrl);

            // wait for the authorization response.
            var context = await http.GetContextAsync();

            var formData = GetRequestPostData(context.Request);

            // Brings the Console to Focus.
            BringConsoleToFront();

            // sends an HTTP response to the browser.
            var response = context.Response;
            var buffer = Encoding.UTF8.GetBytes(ResponseHtml);
            response.ContentLength64 = buffer.Length;
            var responseOutput = response.OutputStream;
            await responseOutput.WriteAsync(buffer, 0, buffer.Length);
            responseOutput.Close();

            var loginResult = await _oidcClient.ProcessResponseAsync(formData, state);

            http.Stop();

            if (loginResult.IsError)
                return new SignInResult(loginResult.IsError, loginResult.Error);

            AccessToken = loginResult.AccessToken;
            RefreshToken = loginResult.RefreshToken;
            IdToken = loginResult.IdentityToken;

            return new SignInResult(loginResult.IsError, Consts.LoginSuccess);
        }

        internal async Task<AccessTokenResult> GetAccessToken()
        {
            if (!IsTokenExpired(AccessToken)) return new AccessTokenResult(AccessToken);

            if(string.IsNullOrEmpty(RefreshToken)) return new AccessTokenResult(error: Exceptions.InvalidRefreshToken);
            var result = await _oidcClient.RefreshTokenAsync(RefreshToken);

            if (result.IsError)
            {
                return new AccessTokenResult(error: result.Error);
            }

            AccessToken = result.AccessToken;
            RefreshToken = result.RefreshToken;
            IdToken = result.IdentityToken;

            return new AccessTokenResult(AccessToken);
        }

        internal string GetRefreshToken()
        {
            return RefreshToken;
        }

        private bool IsTokenExpired(string token)
        {
            if (string.IsNullOrEmpty(token))
            {
                return true;
            }

            try
            {
                var handler = new JwtSecurityTokenHandler();
                var jsonToken = handler.ReadJwtToken(token);
                return jsonToken.ValidTo < DateTime.UtcNow.AddMinutes(-5);
            }
            catch (Exception)
            {
                return true;
            }
        }

        // Hack to bring the Console window to front.

        // ref: http://stackoverflow.com/a/12066376
        [DllImport("kernel32.dll", ExactSpelling = true)]
        private static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool SetForegroundWindow(IntPtr hWnd);

        private void BringConsoleToFront()
        {
            SetForegroundWindow(GetConsoleWindow());
        }

        private string GetRequestPostData(HttpListenerRequest request)
        {
            return request.RawUrl;
        }

        private int GetRandomUnusedPort()
        {
            var listener = new TcpListener(IPAddress.Loopback, 0);
            listener.Start();
            var port = ((IPEndPoint)listener.LocalEndpoint).Port;
            listener.Stop();
            return port;
        }
    }
}
