﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using HowNow.Hub.Exporter.Models.Records;
using HowNow.Hub.Exporter.Models.Clients;
using Serilog;
using HowNow.Hub.Exporter.Clients;
using HowNow.Hub.Exporter.Configuration;
using HowNow.Hub.Exporter.Constants;
using HowNow.Hub.Exporter.Models.Members;

namespace HowNow.Hub.Exporter
{
    public class Exporter
    {
        private string _localStoragePath;
        private StorageFormat _storageFormat;
        private HubClient _hubClient;
        private Stopwatch _exportTimer;
        private List<Client> _clientList;
        private List<Employee> _employeeList;
        private List<SimpleRecordLocation> _locationList;
        private int _result;

        public async Task<int> Run()
        {
            _hubClient = new HubClient();

            Log.Information("+--------------------------------------+");
            Log.Information("|  *Welcome to the Hub File Exporter*  |");
            Log.Information("|                                      |");
            Log.Information("|  Please sign in to Business Fitness  |");
            Log.Information("|    Identity with the firms system    |");
            Log.Information("|           admin user account.        |");
            Log.Information("+--------------------------------------+");
            Log.Information(string.Empty);
            Log.Information("Press any key to sign in...");
            Console.ReadLine();
            Log.Information("Complete login in browser window.");

            var loginResult = await _hubClient.LoginAsync();

            Log.Information($"{loginResult.Message}");
            Log.Information(string.Empty);

            if (loginResult.IsError)
            {
                Log.Error("Login Failed");
                Console.ReadLine();
                return 1;
            }

            CollectUserInput();

            Log.Information("Press any key to start the exporter...");
            Console.ReadLine();

            DateTime localDate = DateTime.Now;
            Log.Information("----------------------------------------------------");
            Log.Information(" Hub File Exporter started at " + localDate);
            Log.Information("----------------------------------------------------");
            Log.Information(string.Empty);
            
            _exportTimer = Stopwatch.StartNew();

            if (!Directory.Exists(_localStoragePath))
                Directory.CreateDirectory(_localStoragePath);

            _result = await GetRecordLocations();

            if (_result == 0) return _result;

            _result = await GetClientData();

            if (_result == 0) return _result;

            _result = await GetEmployeeData();

            if (_result == 0) return _result;

            Log.Information("Fetching records for export...");

            try
            {
                var records = await _hubClient.GetAllRecords();

                await ProcessRecordListAsync(records.Records);

                while (records.CurrentPage != records.MaxPage)
                {
                    records = await _hubClient.GetAllRecords(records.CurrentPage + 1);
                    await ProcessRecordListAsync(records.Records);
                }

                Log.Information($"Processed {records.Total} records in " + _exportTimer.Elapsed);
            }
            catch (Exception e)
            {
                Log.Error(e.Message);
                Console.ReadLine();
                return 0;
            }

            Log.Information("Completed in " + _exportTimer.Elapsed);
            Console.ReadLine();
            return 1;
        }

        private async Task<int> ProcessRecordDownloadAsync(Record record, string filePath)
        {
            var fullFileName = await _hubClient.DownloadRecordById(record.RecordId, filePath);

            return (File.Exists(fullFileName) ? 1 : 0);
        }
        private void CollectUserInput()
        {
            Log.Information("Please select a storage structure for the export:");
            Log.Information(string.Empty);
            Log.Information("Enter 0 for ClientCode");
            Log.Information("Enter 1 for ClientName");
            Log.Information("Enter 2 for ClientCode then LocationPath");
            Log.Information("Enter 3 for ClientName then LocationPath");

            var storageResult = Convert.ToInt32(Console.ReadLine());
            _storageFormat = (StorageFormat)storageResult;

            Log.Information("Storage Format: " + _storageFormat);
            Log.Information(string.Empty);

            Log.Information("Please enter the destination path for exported files:");

            _localStoragePath = Console.ReadLine();

            if (_localStoragePath != null && !_localStoragePath.EndsWith(@"\") && !_localStoragePath.EndsWith("/"))
                _localStoragePath = $@"{_localStoragePath}\";

            Log.Information("Destination Path: '" + _localStoragePath + "'");
            Log.Information(string.Empty);
        }
        private async Task<int> GetRecordLocations()
        {
            var timer = Stopwatch.StartNew();
            _locationList = new List<SimpleRecordLocation>();
            Log.Information("Fetching record locations...");
            try
            {
                var locations = await _hubClient.GetClientRecordLocations();
                
                foreach (var location in locations)
                {
                    _locationList.Add(new SimpleRecordLocation()
                    {
                        Description = location.Description,
                        GroupId = location.GroupId,
                        LocationId = location.LocationId,
                        ParentId = location.LocationId
                    });

                    BuildLocationList(location.SubLocations, location.LocationId);
                }
                
                Log.Information("Completed in " + timer.Elapsed);
                Log.Information(string.Empty);
                return 1;
            }
            catch (Exception e)
            {
                Log.Error(e.Message);
                Console.ReadLine();
                return 0;
            }
        }
        private void BuildLocationList(IEnumerable<RecordLocation> locations, int parentId)
        {
            foreach (var location in locations)
            {
                _locationList.Add(new SimpleRecordLocation()
                {
                    Description = location.Description,
                    GroupId = location.GroupId,
                    LocationId = location.LocationId,
                    ParentId = parentId
                });

                if (location.SubLocations != null)
                    BuildLocationList(location.SubLocations, location.LocationId);
            }
        }
        private async Task<int> GetClientData()
        {
            var timer = Stopwatch.StartNew();
            Log.Information("Fetching client metadata...");
            try
            {
                var clients = await _hubClient.GetAllClientIds(1, null, true);

                _clientList = clients.Clients;

                while (clients.CurrentPage != clients.MaxPage)
                {
                    clients = await _hubClient.GetAllClientIds(clients.CurrentPage + 1, null, true);

                    _clientList.AddRange(clients.Clients);
                }

                Log.Information($"Found {clients.Total} clients in " + timer.Elapsed);
                Log.Information(string.Empty);
                return 1;
            }
            catch (Exception e)
            {
                Log.Error(e.Message);
                Console.ReadLine();
                return 0;
            }
        }

        private async Task<int> GetEmployeeData()
        {
            var timer = Stopwatch.StartNew();
            Log.Information("Fetching employee metadata...");
            try
            {
                _employeeList = await _hubClient.GetEmployeeDetails();

                Log.Information($"Found {_employeeList.Count} employees in " + timer.Elapsed);
                Log.Information(string.Empty);
                return 1;
            }
            catch (Exception e)
            {
                Log.Error(e.Message);
                Console.ReadLine();
                return 0;
            }
        }

        private async Task ProcessRecordListAsync(List<Record> records)
        {
            foreach (var record in records)
            {
                if (!RecordIsValid(record))
                {
                    Log.Information($"Record failed validation. RecordId: {record.RecordId}. File Skipped.");
                    continue;
                }

                

                var recordPath = _localStoragePath;
                var location = _locationList.Find(x => x.LocationId == record.LocationId);
                
                if ((_storageFormat == StorageFormat.ByClientNameThenLocationPath) || (_storageFormat == StorageFormat.ByClientCodeThenLocationPath))
                {
                    recordPath += GetLocationPath(location.LocationId);
                }

                switch ((LocationType)location.GroupId)
                {
                    case LocationType.LocationTypeClient:

                        if (!_clientList.Exists(x => x.ClientId == record.SubLocationId))
                        {
                            Log.Information($"Contact Lookup failed. RecordId: {record.RecordId} " + 
                                            $"SubLocationId: {record.SubLocationId}. File Skipped.");
                            continue;
                        }

                        var clientFolderName = GetClientFolderName(record.SubLocationId);
                        recordPath = recordPath.Replace("%NAME%", clientFolderName);

                        break;

                    case LocationType.LocationTypeEmployee:

                        if (!_employeeList.Exists(x => x.Id == record.SubLocationId))
                        {
                            Log.Information($"Employee Lookup failed. RecordId: {record.RecordId} " +
                                            $"SubLocationId: {record.SubLocationId}. File Skipped.");
                            continue;
                        }
                        var employee = _employeeList.Find(x => x.Id == record.SubLocationId);
                        recordPath = recordPath.Replace("%NAME%", employee.Firstnames + " " + employee.Lastname);

                        break;

                    case LocationType.LocationTypeAdmin:

                        break;
                }

                Log.Information((await ProcessRecordDownloadAsync(record, recordPath) == 1)
                    ? $"Download successful. RecordId: {record.RecordId}. Path: {recordPath}"
                    : $"Download failed. RecordId: {record.RecordId}. Path: {recordPath}");
            }
        }

        private bool RecordIsValid(Record record)
        {
            return !(record.LocationId <= 1) &&
                   record.SubLocationId != -1 &&
                   _locationList.Exists(x => x.LocationId == record.LocationId);
        }

        private string GetClientFolderName(int? clientId)
        {
            var client = _clientList.Find(x => x.ClientId == clientId);
            string folder;

            switch (_storageFormat)
            {
                case StorageFormat.ByClientCode:
                case StorageFormat.ByClientCodeThenLocationPath:
                    folder = client.ClientCode;
                    break;

                case StorageFormat.ByClientName:
                case StorageFormat.ByClientNameThenLocationPath:
                    folder = client.EntityName;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return folder;
        }

        private string GetEmployeeFolderName(int? employeeId)
        {
            var employee = _clientList.Find(x => x.ClientId == employeeId);
            var folder = employee.ClientCode;

            return folder;
        }

        private string GetLocationPath(int? locationId)
        {
            var location = _locationList.Find(x => x.LocationId == locationId);
            var path = string.Empty;

            while (location.LocationId != location.ParentId)
            {
                path = path.Insert(0, $@"{location.Description}\");
                location = _locationList.Find(x => x.LocationId == location.ParentId);
            }

            if (location.GroupId != Consts.LocationTypeAdmin)
                path = path.Insert(0, $@"{location.Description}\%NAME%\");

            return path;
        }
    }
}
