﻿namespace HowNow.Hub.Exporter.Constants
{
    internal class Exceptions
    {
        public const string InvalidRefreshToken = "Invalid refresh token, please sign in again.";
        public const string FileNotFound = "File [{0}] not found.";
        public const string RecordLocationNotFound = "Record location not found.";
    }
}
