﻿namespace HowNow.Hub.Exporter.Constants
{
    internal class Consts
    {
        public const string Authorization = "Authorization";
        public const string Bearer = "Bearer";
        public const string RecordContent = "Record";
        public const string FileContent = "File";
        public const string LoginSuccess = "Successfully logged in.";
        public const string StoringByClientName = "Storing files by client name";
        public const string StoringByClientCodeThenFilter = "Storing files by client code then location path";
        public const string StoringByClientNameThenFilter = "Storing files by client name then location path";

        public const int LocationTypeClient = 1;
        public const int LocationTypeEmployee = 2;
        public const int LocationTypeAdmin = 3;
    }
}
