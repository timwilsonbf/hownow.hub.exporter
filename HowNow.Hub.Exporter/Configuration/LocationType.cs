﻿namespace HowNow.Hub.Exporter.Configuration
{
    public enum LocationType
    {
    LocationTypeClient = 1,
    LocationTypeEmployee = 2,
    LocationTypeAdmin = 3
    }
}