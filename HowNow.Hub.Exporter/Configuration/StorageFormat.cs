﻿namespace HowNow.Hub.Exporter.Configuration
{
    public enum StorageFormat
    {
        ByClientCode,
        ByClientName,
        ByClientCodeThenLocationPath,
        ByClientNameThenLocationPath
    }
}